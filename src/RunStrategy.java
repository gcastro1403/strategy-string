
public class RunStrategy {

	public static void main(String[] args) {
		LowerStrategyTextFormatter lower = new LowerStrategyTextFormatter();
		CapitalStrategyTextFormatter upper = new CapitalStrategyTextFormatter();
		
		App appLower = new App(lower);
		appLower.run("MINUSCULAS");
		
		App appUpper = new App(upper);
		appUpper.run("mayusculas");
		
		//Output console
		
		//minusculas
		//MAYUSCULAS

	}

}
