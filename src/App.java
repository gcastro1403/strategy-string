
public class App {
	
	private Logger logger;
	
	public App(Logger logger) {
		
		this.logger = logger;
	}
	
	public void run(String text) {
		
		logger.write(text);
		
	}
}
